package rest;


import java.net.HttpURLConnection;
import java.net.URL;

public class Zad4_Delete {
    Zad4_Delete () throws Exception{
        // The java URL connection to the resource.
        URL url = new URL("http://admin:admin@localhost:8984/rest/mondial");

        // Establish the connection to the URL.
        HttpURLConnection conn = (HttpURLConnection) url.openConnection();
        // Set as DELETE request.
        conn.setRequestMethod("DELETE");


        // Print the HTTP response code.
        System.out.println("HTTP DELETE response: " + conn.getResponseCode());

        // Close connection.
        conn.disconnect();
    }
}
