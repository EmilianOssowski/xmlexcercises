package rest;

import java.io.*;
import java.net.HttpURLConnection;
import java.net.URL;

public class Zad2_Create {
    public Zad2_Create () throws IOException {
        URL url = new URL("http://admin:admin@localhost:8984/rest/mondial");
        HttpURLConnection conn = (HttpURLConnection) url.openConnection();
        conn.setDoOutput(true);
        conn.setRequestMethod("PUT");
        try(OutputStream out = new BufferedOutputStream(conn.getOutputStream());
            InputStream in = new BufferedInputStream(
                    new FileInputStream("mondial.xml"))) {
            // Send document to server
            System.out.println("\n* Send document...");
            for(int i; (i = in.read()) != -1;) out.write(i);
        }
        System.out.println("\n* HTTP response: " + conn.getResponseCode() +
                " (" + conn.getResponseMessage() + ')');
        conn.disconnect();
        System.out.println("done");

    }
}
