package rest;

import local.Queries;
import server.Zad3_Timer;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;

public class Zad3_QueryPOST {
    Zad3_QueryPOST() throws Exception {
        executeQuery(Queries.ZAD6.toString());
        executeQuery(Queries.ZAD7.toString());
        executeQuery(Queries.ZAD8.toString());
        executeQuery(Queries.ZAD9.toString());
        executeQuery(Queries.ZAD10.toString());
    }

    void executeQuery(String zad) throws Exception {
        Thread t = new Thread(() -> {
            Zad3_Timer timer = new Zad3_Timer();
            try {
                System.out.println("=== POST: execute a query ===");

                // The java URL connection to the resource
                URL url = new URL("http://admin:admin@localhost:8984/rest/mondial");
                System.out.println("\n* URL: " + url);

                // Query to be sent to the server
                String request =
                        "<query xmlns='http://basex.org/rest'>\n" +
                                "  <text>" + zad + "</text>\n" +
                                "</query>";
                System.out.println("\n* Query:\n" + request);

                // Establish the connection to the URL
                HttpURLConnection conn = (HttpURLConnection) url.openConnection();
                // Set an output connection
                conn.setDoOutput(true);
                // Set as PUT request
                conn.setRequestMethod("POST");
                // Specify content type
                conn.setRequestProperty("Content-Type", "application/query+xml");

                // Get and cache output stream
                try (OutputStream out = conn.getOutputStream()) {
                    // Send UTF-8 encoded query to server
                    out.write(request.getBytes("UTF-8"));
                }

                // Print the HTTP response code
                int code = conn.getResponseCode();
                System.out.println("\n* HTTP response: " + code +
                        " (" + conn.getResponseMessage() + ')');

                // Check if request was successful
                if (code == HttpURLConnection.HTTP_OK) {
                    // Print the received result to standard output (same as GET request)
                    System.out.println("\n* Result:");

                    // Get and cache input as UTF-8 encoded stream
                    try (BufferedReader br = new BufferedReader(
                            new InputStreamReader(conn.getInputStream(), "UTF-8"))) {

                        // Print all lines of the result
                        for (String line; (line = br.readLine()) != null; ) {
                            System.out.println(line);
                        }
                    }
                }

                // Close connection
                conn.disconnect();
            }catch (Exception e){
                System.out.println(e);
            }
            finally {
                timer.stop(zad);
            }
        });
        t.run();
    }
}
