package rest;

public class Zad5_Execute {
    public Zad5_Execute() throws Exception{
        Zad1_server zad1_server= new Zad1_server();
        Zad2_Create zad2_create = new Zad2_Create();
        Zad3_QueryGET zad3_queryGET = new Zad3_QueryGET();
        Zad3_QueryPOST zad3_queryPOST = new Zad3_QueryPOST();
        Zad4_Delete zad4_delete = new Zad4_Delete();

        zad1_server.stopServer();
    }
}
