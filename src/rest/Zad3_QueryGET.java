package rest;

import local.Queries;
import server.Zad3_Timer;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;

public class Zad3_QueryGET {
    Zad3_QueryGET(){
        executeQuery(Queries.ZAD6.toString().replace(" ", "%20"));
        executeQuery(Queries.ZAD7.toString().replace(" ", "%20"));
        executeQuery(Queries.ZAD8.toString().replace(" ", "%20"));
        executeQuery(Queries.ZAD9.toString().replace(" ", "%20"));
        executeQuery(Queries.ZAD10.toString().replace(" ", "%20"));

    }
    private void executeQuery(String zad){
        Thread t = new Thread( ()->{
            Zad3_Timer timer = new Zad3_Timer();
            try {

                System.out.println("=== GET: execute a query ===");

                // The java URL connection to the resource
                String base = "http://localhost:8984/rest/";

                URL url = new URL(base + "mondial?query="+zad);
                System.out.println("\n* URL: " + url);

                // Establish the connection to the URL
                HttpURLConnection conn = (HttpURLConnection) url.openConnection();

                // Print the HTTP response code
                int code = conn.getResponseCode();
                System.out.println("\n* HTTP response: " + code +
                        " (" + conn.getResponseMessage() + ')');

                // Check if request was successful
                if (code == HttpURLConnection.HTTP_OK) {
                    // Print the received result to standard output
                    System.out.println("\n* Result:");

                    // Get and cache input as UTF-8 encoded stream
                    try (BufferedReader br = new BufferedReader(
                            new InputStreamReader(conn.getInputStream(), "UTF-8"))) {

                        // Print all lines of the result
                        for (String line; (line = br.readLine()) != null; ) {
                            System.out.println(line);
                        }
                    }
                }
                conn.disconnect();

            }catch (Exception e){
                System.out.println(e);
            }
            finally {
                timer.stop(zad);
            }
        });
        t.run();
    }



}
