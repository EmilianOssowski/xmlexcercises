package server;

public class Zad3_Timer {
    long startTime;
    long endTime;

    public Zad3_Timer() {
        startTime = System.currentTimeMillis();
    }

    public void stop(String param) {
        endTime = System.currentTimeMillis() - startTime;
        System.out.println("Estimated time " + param + ": "+ endTime + " ms");
    }
}
