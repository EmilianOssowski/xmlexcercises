package server;

import org.basex.BaseXServer;
import org.basex.api.client.ClientSession;
import org.basex.core.cmd.CreateDB;

import java.io.IOException;

public class Zad1_Conn {
    BaseXServer server = null;
    ClientSession session;

    void connect() {
        try {
            server = new BaseXServer();
        } catch (IOException e) {
            e.printStackTrace();
        }
        try {
            session = new ClientSession("localhost", 1984, "admin", "admin");
            session.execute(new CreateDB("db", "mondial.xml"));

        } catch (IOException e) {
            e.printStackTrace();
        }
    }
    void close() {
        server.stop();
    }

}

