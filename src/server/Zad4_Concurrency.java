package server;

import local.Queries;
import local.Zad2_Execute;

import java.io.IOException;

public class Zad4_Concurrency {


    public Zad4_Concurrency(){
        Zad1_Conn zad1_Conn = new Zad1_Conn();
        zad1_Conn.connect();
        executeOne();
        executeAll();
        zad1_Conn.close();
    }

    void executeOne(){
        Zad2_ExecuteQueryThread exec = new Zad2_ExecuteQueryThread();
        Zad3_Timer timer = new Zad3_Timer();
        try {
            exec.execQuery(Queries.ZAD6.toString());
        }catch (IOException e){
            e.printStackTrace();
        }
        try {
            exec.execQuery(Queries.ZAD7.toString());

        }catch (IOException e){
            e.printStackTrace();
        }
        try {
            exec.execQuery(Queries.ZAD8.toString());

        }catch (IOException e){
            e.printStackTrace();
        }
        try {
            exec.execQuery(Queries.ZAD9.toString());
        }catch (IOException e){
            e.printStackTrace();
        }
        try {
            exec.execQuery(Queries.ZAD10.toString());
        }catch (IOException e){
            e.printStackTrace();
        }
        timer.stop("One Querry time: ");



    }
    void executeAll(){
        Zad2_ExecuteQueryThread zad6 = new Zad2_ExecuteQueryThread();
        Zad2_ExecuteQueryThread zad7 = new Zad2_ExecuteQueryThread();
        Zad2_ExecuteQueryThread zad8 = new Zad2_ExecuteQueryThread();
        Zad2_ExecuteQueryThread zad9 = new Zad2_ExecuteQueryThread();
        Zad2_ExecuteQueryThread zad10 = new Zad2_ExecuteQueryThread();

        try {
            Zad3_Timer timer = new Zad3_Timer();
            zad6.execQuery(Queries.ZAD6.toString());
            zad7.execQuery(Queries.ZAD7.toString());
            zad8.execQuery(Queries.ZAD8.toString());
            zad9.execQuery(Queries.ZAD9.toString());
            zad10.execQuery(Queries.ZAD10.toString());
            timer.stop("All Queries time: ");
        }catch (IOException e){
            e.printStackTrace();
        }
    }
}
