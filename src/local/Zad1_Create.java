package local;

import org.basex.core.*;
import org.basex.core.cmd.*;

public class Zad1_Create {
	private Context context = new Context();
	private String dbName;
	private String dbFile;

	public Zad1_Create(String dbName, String dbFile) {
		super();
		this.dbName = dbName;
		this.dbFile = dbFile;
	}

	public void createDb() throws BaseXException {
		System.out.println("\n* Create a database.");
		//new Set("intparse", true).execute(context);
		new CreateDB(dbName, dbFile).execute(context);
		System.out.println("\n* Show existing databases:");
	    System.out.print(new List().execute(context));
		//context.close();
	}

	public void removeDb() throws BaseXException {
		System.out.println("\n* Drop the database.");
		new DropDB(dbName).execute(context);
		System.out.println("\n* Show existing databases:");
	    System.out.print(new List().execute(context));
		context.close();
	}

	public String getDbName() {
		return dbName;
	}

	public void setDbName(String dbName) {
		this.dbName = dbName;
	}

	public String getDbFile() {
		return dbFile;
	}

	public void setDbFile(String dbFile) {
		this.dbFile = dbFile;
	}

	public Context getContext() {
		return context;
	}

	public void setContext(Context context) {
		this.context = context;
	}

}
