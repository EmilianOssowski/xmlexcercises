package local;

import org.basex.core.BaseXException;

public class Zad3_Run {

	public Zad3_Run() throws BaseXException {

		Zad1_Create zad1 = new Zad1_Create("new", "C:\\mondial.xml");
		zad1.createDb();
		Zad2_Execute exec = new Zad2_Execute(zad1.getContext());
		
		System.out.println("1");
		exec.query(Queries.ZAD1.toString());
		System.out.println();
		System.out.println("2");
		exec.query(Queries.ZAD2.toString());
		System.out.println();
		System.out.println("3");
		exec.query(Queries.ZAD3.toString());
		System.out.println();
		System.out.println("4");
		exec.query(Queries.ZAD4.toString());
		System.out.println();
		System.out.println("5");
		exec.query(Queries.ZAD5.toString());
		System.out.println();
		System.out.println("6");
		exec.query(Queries.ZAD6.toString());
		System.out.println();
		System.out.println("7");
		exec.query(Queries.ZAD7.toString());
		System.out.println();
		System.out.println("8");
		exec.query(Queries.ZAD8.toString());
		System.out.println();
		System.out.println("9");
		exec.query(Queries.ZAD9.toString());
		System.out.println();
		System.out.println("10");
		exec.query(Queries.ZAD10.toString());
		
		zad1.removeDb();
	}

}
