package local;

import java.io.*;

import org.basex.core.*;
import org.basex.core.cmd.*;
import org.basex.io.serial.*;
import org.basex.query.*;
import org.basex.query.iter.*;
import org.basex.query.value.*;
import org.basex.query.value.item.*;

public class Zad2_Execute {
	private Context context;

	public Zad2_Execute(Context context) {
		super();
		this.context = context;
	}

	public void query(final String query) throws BaseXException {
		System.out.println(new XQuery(query).execute(context));
	}
}
