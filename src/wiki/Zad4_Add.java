package wiki;

import org.basex.core.BaseXException;
import org.basex.core.Context;
import org.basex.core.cmd.Set;

import local.Zad1_Create;
import local.Zad2_Execute;

public class Zad4_Add {

	public Zad4_Add () throws BaseXException {
		Context context = new Context();
		new Set("intparse", true).execute(context);
		final String doc = "http://en.wikipedia.org/wiki/Albert_Einstein";
		Zad1_Create zad1 = new Zad1_Create("wiki", doc);
		zad1.setContext(context);
		zad1.createDb();
		Zad2_Execute exec = new Zad2_Execute(context);
		
		exec.query("declare namespace xhtml='http://www.w3.org/1999/xhtml';" +
        	"for $x in //*[@id='mw-content-text']/table[3]" +
        	"where $x contains text ('edit.*' ftand ('article' ftor 'page')) " +
        	"  using wildcards distance at most 10 words " +
        	"return <p>{ $x }</p>");
		
		zad1.removeDb();
	}
}
